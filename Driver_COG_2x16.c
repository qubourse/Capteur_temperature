/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Octobre 2016                                                            */
/*                                                                          */
/*        Driver pour Gestion d'un afficheur COG alpha numerique            */
/*        compatible controleur Hitachi HD44780, interface 4 bits           */
/*                                                                          */
/* Driver_COG_H44780.c                                       PIC 18F542     */
/****************************************************************************/
#define Driver_COG_2x16_C



#include <p18cxxx.h>
#include "Constantes.h"
#include "Delais.h"
#include "Driver_COG_2x16.h"
#include "Driver_PE_23S17.h"

// Format de la connectique :  6 bits sur port PORTB
//       Data(7:4) = PORTB(3:0)
//       RS = PORTB(4)
//       E = PORTB(5)

//================================================================   
//      Fonctions d'envoi de commandes ou de donnees
//================================================================

// Les commandes doivent toutes etre separees d'un delai de 5ms (au moins 4ms)
// Les quartets ou octets doivent etre separes par un "pulse" sur la 
// broche COG_E (PORTB(5)), d'une duree d'1 micro seconde (au moins 450 ns)

// Envoi d'une commande en mode 8 bits : RS = 0 ( = PORTB(4))
// Attention : seuls les 4 bits de poids forts sont envoyes sur PORTB(3:0)

int valeur;




void COG_SendCmd8(unsigned char Instruction) {
  
  // Recuperation des 3 bits PORTB(7:5) (pour ne pas les ecraser)
 // Commande_Emise = (PORTB & 0b11100000);
  // Puis ajout des 4 bits forts de l'instruction (sur les poids faibles)
  // car on doit faire : PORTB(3:0) <- Data(7:4)
 // Commande_Emise |= ((Instruction)>>4)&0x0F;
  // Emission effective de cette commande
 // LATB = Commande_Emise;
  
  
   valeur=(valeur & 0b00001011);
   valeur |= ((Instruction))&0xF0;
   PE_PORT1WriteByte(valeur);
   Delai_10us();
  
  
  // Impulsion sur "Enable" pour valider cette commande
  COG_Pulse_E();
  // Attente obligatoire avant re-envoi d'une autre commande
  Delai_5ms();
}


// Envoi d'une commande en mode 4 bits : RS = 0 ( = PORTB(4))
// Transfert Big Endian : D(7:4)= poids fort puis D(7:4)= poids faible
void COG_SendCmd(unsigned char Instruction) {

	valeur=(valeur & 0b00001011);
	valeur|=((Instruction)&0xF0);
	PE_PORT1WriteByte(valeur);
	Delai_10us();
	COG_Pulse_E();
	valeur=(valeur & 0b00001011);
	valeur|=((Instruction<<4)&0xF0);
	PE_PORT1WriteByte(valeur);
	Delai_10us();
	COG_Pulse_E();
}

// Envoi d'une donnee en mode 4 bits : RS = 1 ( = PORTB(4))
// Transfert Big Endian : D(7:4)= poids fort puis D(7:4)= poids faible
void COG_SendData(unsigned char Donnee) {
  
    valeur=(valeur & 0b00001011);
	valeur|=((Donnee)&0xF0);
	PE_PORT1WriteByte(valeur);
	Delai_10us();
	COG_Pulse_E();
	valeur=(valeur & 0b00001011);
	valeur|=((Donnee<<4)&0xF0);
	PE_PORT1WriteByte(valeur);
	Delai_10us();
	COG_Pulse_E();
 
}

// Generation d'un front sur la broche COG_E (PORTB(5)) servant a la validation
void COG_Pulse_E(void) { 
	//COG_E = 1;
	
	PE_PORT1WriteByte(0b00001000 | valeur);//COG_E = 1;   // PORTB|=0b00100000;
	Delai_1us(); // attente d'au moins 450 ns (1 micro seconde)
	// COG_E = 0;
	PE_PORT1WriteByte(0b11110111 | valeur); //COG_E = 0;   //PORTB&=0b11011111; // front descendant effectif
}


//================================================================   
//  Initialisation du COG externe sur plateforme MikroElectronika
//================================================================

void COG_Init(void) {
  // Initialisation generale : signaux de controle et de donnees en sortie
  // PORTB(5:0) en sortie, par mise a zero
  //TRISB &= 0b11000000; 
  // Mise a 0 des broches PORTB(5:0), soit RS, E ainsi que les donnees
  //LATB = PORTB & 0b11000000; 
  // Attente d'au moins 15ms pour permettre le demarrage du COG : "Power On"

  
  PE_PORT1WriteByte(0b00000000);
  
  Delai_10ms();
  Delai_6ms();
   
  // 0x30 : Forcage du COG en mode 8 bits, 3 fois de suite (avec RS=0)
  COG_SendCmd8(COG_EIGHT_BIT);
  COG_SendCmd8(COG_EIGHT_BIT);
  COG_SendCmd8(COG_EIGHT_BIT);
  
  // 0x20 : Passage en mode 4 bits
  COG_SendCmd8(COG_FOUR_BIT);
  
  // A partir de maintenant, les commandes doivent etre passees en mode 4 bits Big Endian
  // ------------------------------------------------------------------------------------
  
  // 0x28 : Taille interface, Nombre de lignes et Taille fonte
  //        4 bits (DL=0), 2 lignes (N=1), caracteres 5x7 (N=0)
  COG_SendCmd(COG_TWO_LINES_5x7);

  // 0x0C : Mise en marche de l'affichage (sans curseur ni clignotement)
  COG_SendCmd(COG_DISP_ON);

  // 0x06 : Le curseur laisse le texte a gauche, l'affichage n'accompagne pas le deplacement
  COG_SendCmd(COG_SHIFT_DISP_LEFT);
  
  // 0x80: Positionne la memoire d'affichage en adresse A(6:0)=000 0000
  COG_SendCmd(COG_SET_DDRAM_ADDRESS|0x00);
  
  // 0x01 : "Clear Display"
  COG_SendCmd(COG_CLEAR);
}

//================================================================   
//               Fonctions d'ecriture sur COG
//================================================================

// Positionne l'afficheur a l'adresse DD RAM indiquee en ligne/colonne
void COG_SetAddress(char row, char column) {
	
	row&= 0x01;
	column&= 0x0f;
	
	row<<=2;
		
	COG_SendCmd(COG_SET_DDRAM_ADDRESS|row<<4|column);
 
}

// Ecriture d'un texte fixe, stocke en ROM, a la position indiquee
void COG_WriteAt(char row, char column, const rom char *text) {  
  COG_SetAddress(row,column); // Positionne effectivement
  while(*text) {
    COG_Chr(*text); // Ecriture effective sur COG
    text++;         // Increment
  }
}

// Ecriture d'un texte fixe, stocke en ROM, a la position courante
void COG_Write(const rom char *text) { 
  while(*text) {
    COG_Chr(*text); // Ecriture effective sur COG
    text++;         // Increment
  }
}

// Affichage d'un texte variable, stocke en RAM, a la position indiquee
void COG_PrintAt(char row, char column, char *text) { 
  COG_SetAddress(row,column); // Positionne effectivement
  while(*text) {
    COG_Chr(*text); // Ecriture effective sur COG
    text++;         // Increment
  }
}

// Affichage d'un texte variable, stocke en RAM, a la position courante
void COG_Print(char *text) { 
  while(*text) {
    COG_Chr(*text); // Ecriture effective sur COG
    text++;         // Increment
  }
}

// Affichage d'un caractere, stocke en RAM ou en ROM, a la position indiquee
void COG_ChrAt(char row, char column, char out_char) { // Affichage d'un caractere en ligne et colonne
  COG_SetAddress(row,column); // Positionne effectivement
  COG_SendData(out_char);     // Ecriture effective du caractere a cet endroit
}

// Affichage d'un caractere, stocke en RAM ou en ROM, a la position courante
// defini par #define COG_Chr(Caractere) COG_SendData(Caractere)

