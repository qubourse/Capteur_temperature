/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2016                                                          */
/*                                                                          */
/*                                                                          */
/*                     Driver pour Expander de Ports (PE)                   */
/*                     MicroChip MCP 23S17 en mode SPI                      */
/*                                                                          */
/* Driver_PE_23S17.c                                         PIC 18F542     */
/****************************************************************************/


#define _Driver_PE_23S17_C

#include <p18cxxx.h>
#include <spi.h> // Pour l'acces en SPI
#include "Constantes.h"
#include "Delais.h"
#include "Driver_PE_23S17.h"


void PE_Init(void) {
	//orientation signaux de controle du port expander
	TRIS_PE_CS  = 0;
	TRIS_PE_RST = 0;
	//init valeurs par defauts
	PE_CS  = 1;
	PE_RST = 1;
	//configuration et ouverture du SPI (mssp)
	OpenSPI(SPI_FOSC_4,MODE_00,SMPMID);
	//Impulsion de mise en route
	PE_RST = 0;
	Delai_1us();
	PE_RST = 1;
}

void PE_SendCmd(INT8U Instruction, INT8U Parametre) {
	PE_CS = 0; //selection de la puce
	WriteSPI(0x40 |PE_ADDRESS<<1); //Mot de controle : 0100 A2 A1 A0 RW avec RW = 0 pour Write
	WriteSPI(Instruction);
	WriteSPI(Parametre);
	PE_CS = 1; //Stop la communication
}

void PE_PORT0SetMode(INT8U Orientation) { //IODIRA = Orientation
	PE_SendCmd(IODIRA,Orientation);
}

void PE_PORT1SetMode(INT8U Orientation) { //IODIRB = Orientation
	PE_SendCmd(IODIRB,Orientation);
}

void PE_PORT0WriteByte(INT8U Valeur) { //OLATA = valeur 
	PE_SendCmd(OLATA,Valeur);
}

void PE_PORT1WriteByte(INT8U Valeur) { //OLATB = valeur 
	PE_SendCmd(OLATB,Valeur);
}
