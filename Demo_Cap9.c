/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Octobre 2015                                                            */
/*                                                                          */
/*           Programme simple de test de l'afficheur GLCD 320x240           */
/*                                                                          */
/* Demo_Cap9.c                                              ATMEL AT91CAP9  */
/****************************************************************************/



// Inclusions des bibliotheques des fonctions
// ------------------------------------------

#include "Driver_HSAt.h"

//    Carte : board (board_memories)
#include <board.h>
//#include <board_memories.h>

//    Broches d'E/S :  (pio)
#include <pio/pio.h>

//    Outils de debug : trace (dbgu, stdio, string)
#include <utility/trace.h>
//#include <dbgu/dbgu.h>
#include <stdio.h>
//#include <string.h>

//    Ecran GLCD : lcdd, draw, color
#include <lcd/lcdd.h>
#include <lcd/draw.h>
#include <lcd/color.h>
#include <lcd/font.h>


//    TouchScreen TSD : tsd, irq
#include <tsd/tsd.h>
#include <irq/irq.h>
//    Temps
#include <rtt/rtt.h>


// Definition de macros
// --------------------
#define min(A,B) ((A)<(B)) ? (A) : (B)
#define max(A,B) ((A)>(B)) ? (A) : (B)


// Variable locale en RAM EXTerne : SDRAM
// --------------------------------------
// A noter : la base de la memoire SDRAM est AT91C_EBI_SDRAM (= 0x70000000)

// Adresse de l'image destinee au LCD (stockee par defaut en 0x7010000)
static unsigned char *pLcdImage = (unsigned char *) (AT91C_EBI_SDRAM+ 0x00100000);
// Zone memoire temporaire utilisee notamment pour la calibration de l'ecran
static unsigned char *pLcdBuffer = (unsigned char *) (AT91C_EBI_SDRAM + 0x00200000);


// Variable locale en RAM programme : SRAM
//----------------------------------------
static unsigned int XX,YY; // Positions du point de contact sur le TSD
static unsigned char NouvellesValeurs; // Flag pour nouvelles valeurs de x et y

//Selection Ecran
static unsigned char SelectScreen; // Flag pour connaitre la vue
int i;
int pause = 0;
int tabT[167];
float Temp=60;
int compteur=0;
float val = 0;
short int abs = 0;
short int signe = 0;

// Prototypes 
void draw_T(int xbas,int ybas);
void draw_C(int xbas, int ybas);
void draw_deg(int xbas,int ybas);
void draw_parferm(int xbas,int ybas);
void draw_parouvr(int xbas,int ybas);
void draw_t(int xbas,int ybas);
void draw_s(int xbas,int ybas);


// Programme principal
// -------------------
int main(void) {
  
	
  int ResultatCalibration; // Info de retour pour le TSD
  unsigned int Instant;    // Pour connaitre le temps ecoule
  
  
	HS_Init();
  
  

  // Activation du mode debug (DBGU)
  // ------------------------------
  TRACE_CONFIGURE(DBGU_STANDARD, 115200, BOARD_MCK); // Port serie en 115200 bauds
  //  Master Clock = BOARD_MCK = 100 MHz
  //  Processor Clock = BOARD_PCK = 200 MHz
  //  A noter : Cette activation permet : 
  //             * les "printf" sur l'interface de debug,
  //             * ou mieux : TRACE_DEBUG(), TRACE_INFO(), TRACE_WARNING(), 
  //                         TRACE_ERROR(), TRACE_FATAL()     (Cf. trace.h)
  //             * la lecture sur le port serie, donc, sur le clavier 
  //               par les fonctions : DBGU_GetChar ou TRACE_GetInteger

  // Quelques affichages sur l'interface de debogage
  printf("%c[2J\r", 27); // Effacement de l'ecran debug par scrolling...
  printf("******************************************************\n\r");
  printf("*  PlanB inc.       / Plan B                  ISIMA  *\n\r");
  printf("*  Octobre 2015                                      *\n\r");
  printf("*                                                    *\n\r");
  printf("*             Demonstration 'Temperature'            *\n\r");
  printf("*                                                    *\n\r");
  printf("******************************************************\n\r");
  printf("\n\r Version de la carte : %s", BOARD_NAME);
  printf("\n\r Version de la bilbiotheque : %s", SOFTPACK_VERSION);
  printf("\n\r Version de l'application : %s %s\n\r", __DATE__, __TIME__);
  printf("\n\r\n\r");

  // Initialisation du GLCD de 240x320
  // ---------------------------------
  //    A noter :  240x320 = 76800 pixels, donc 230400 bytes (0x38400)
  //               zone memoire atteinte = SDRAM[0x70000000] a SDRAM[0x700383FF]
  //               6 bits par couleur : 2 exp(18) = 256 K couleurs
  LCDD_Initialize();
  printf(" -> GLCD initialise...\n\r");  
  
  // Initialisation du TSD (TouchScreen Display)
  TSD_Initialize(0); // Sans demande de calibration
  // Ou bien : TSD_Initialize(pLcdBuffer); // Avec calibration integree, mais 
  // sans les affichages de debug...
  
  ResultatCalibration = 0;
  

  
  while (0==ResultatCalibration) { // Calibration avec affichage de debug
    ResultatCalibration = TSD_Calibrate(pLcdBuffer);
    if (ResultatCalibration) {
      printf(" -> Calibration TSD reussie, TSD initialise...\n\r");
    } else {
      printf(" -> Echac Calibration TSD : Erreur trop importante... A refaire\n\r");
    }
  }
  


  
  // Configuration de la base de temps
  // ---------------------------------
  RTT_SetPrescaler(AT91C_BASE_RTTC, 0x8000); // Periode d'1 seconde
  //    A noter :  Ici, utilisation sans mecanisme d'interruption
  //               voir 'basic-rtt-project' pour installation
  //               et exploitation de ISR_Rtt()

  
  // Initialisation des variables
  // ----------------------------
  XX = 0;  // Coordonnees recues
  YY = 0;
  NouvellesValeurs = 0; // Flag de presence d'un couple X, Y
  SelectScreen = 1; // Démarrage sur l'ecran d'accueil
  LCDD_Fill(pLcdBuffer, COLOR_WHITE); // Vidage du buffer temporaire
  printf(" -> Variables initialisees...\n\r");  
  
  
																							//C'est ici qu'on commence à coder
  
  
  // Affichage de l'image pre-chargee
  // --------------------------------
  
  LCDD_DisplayBuffer(pLcdImage);
  printf(" -> Affichage termine...\n\r"); 
      
  printf("\n");

  
  // Boucle infinie !
  // ----------------
  printf(" -> Attente d'appui sur le TouchScreen\n\r"); 
  while (1) {
	Instant = RTT_GetTime(AT91C_BASE_RTTC);
	while(Instant == RTT_GetTime(AT91C_BASE_RTTC)); // Attente active d'une seconde
	
	if (NouvellesValeurs) {
      printf("[x,y] = [%d,%d] au bout de %d secondes\n\r", XX, YY, Instant);
	  NouvellesValeurs = 0;
	}
	XX++;
	//printf("%d ",XX);
	if(!SelectScreen){
		AffPlanB();
	}
  }
  
  printf("\n\r * ATTENTION * : BUG Systeme car sortie de l'application !! \n\r"); 
  return 0;
}


// Fonction utilitaire pour conversion des couleurs
// Conversion mode de couleur  "Hue Saturation Value" vers "Red Green Blue"
//    Format : Hue, Saturation, Value_Brightness -> Red, Green, Blue
//             (0-360,0-100,0-100) -> Color(255,255,255)
unsigned int HSV2RGBColor(int H, int S, int V) {
  unsigned int CouleurRetournee; // Valeur de sortie
  float hue, sat, val; // Valeurs d'entree
  float red, green, blue; // Valeurs de calcul
  unsigned char RR, GG, BB; // Valeurs en octets
  int sextant; // Partie entiere de Hue (ti)
  float fract; // Partie fractionnaire de Hue (f)
  float l, m, n, v; // valeurs pour conversion
	
  // Conversion vers les bons intervalles des espaces couleurs
  hue = max(0, min(360, H)) /  60.0; // intervalle 0..6
  sat = max(0, min(100, S)) / 100.0; // intervalle 0..1
  val = max(0, min(100, V)) / 100.0; // intervalle 0..1
 
  // Initialisation en cas particulier : Noir
  red = 0; green = 0; blue = 0;
  if (sat == 0) { // Cas achromatique : niveau de gris                                                                  
    red = val; green = val; blue = val;
  } else { // Cas chromatique : calcul de la couleur
    sextant = (int) (hue);  // ti
    fract = hue - sextant; // f
    l = val * (1 - sat);
    m = val * (1 - sat * fract);
    n = val * (1 - sat * (1 - fract));
    v = val;	 
    switch (sextant) {
      case 0: red = v; green = n; blue = l; break;
      case 1: red = m; green = v; blue = l; break;
      case 2: red = l; green = v; blue = n; break;
      case 3: red = l; green = m; blue = v; break;
      case 4: red = n; green = l; blue = v; break;
      case 5: red = v; green = l; blue = m; break;
    }
  }
  RR = (unsigned char) (red * 255);
  GG = (unsigned char) (green * 255);
  BB = (unsigned char) (blue * 255);
  CouleurRetournee = RR<<16 | GG <<8 | BB;
  return (CouleurRetournee);
}

// Fonction d'affichage

void AffPlanB(){
	int dg = -10;
	int db = -80;
	int i=0;
	int diz=0;
	int uni=0;
	int virg=0;
	
	//Réinitialisation du screen
	
	LCDD_DrawRectangle(pLcdBuffer,80,220,60,80,COLOR_WHITE);
	
	//Affichage bouton pause
	if (!pause){
	LCDD_DrawRectangle(pLcdBuffer,180,260,50,50,COLOR_BLACK);
	LCDD_DrawRectangle(pLcdBuffer,190,270,30,10,COLOR_WHITE);
	LCDD_DrawRectangle(pLcdBuffer,190,290,30,10,COLOR_WHITE);
	}
	else{
	LCDD_DrawRectangle(pLcdBuffer,180,260,50,50,COLOR_BLACK);
	LCDD_DrawRectangle(pLcdBuffer,200,270,10,10,COLOR_WHITE);
	LCDD_DrawRectangle(pLcdBuffer,195,280,20,10,COLOR_WHITE);
	LCDD_DrawRectangle(pLcdBuffer,190,290,30,10,COLOR_WHITE);
	}
	
	
	
	
	/*//Affichage Zoom +
	LCDD_DrawRectangle(pLcdBuffer,100-db,280-dg,20,20,COLOR_BLACK);
	LCDD_DrawRectangle(pLcdBuffer,103-db,289-dg,14,2,COLOR_WHITE);
	LCDD_DrawRectangle(pLcdBuffer,109-db,283-dg,2,14,COLOR_WHITE);
	
	//Affichage Zoom -
	LCDD_DrawRectangle(pLcdBuffer,100-db,250-dg,20,20,COLOR_BLACK);
	LCDD_DrawRectangle(pLcdBuffer,109-db,253-dg,2,14,COLOR_WHITE);
	
	
	//Affichage Fleche Gauche
	LCDD_DrawRectangle(pLcdBuffer,130-db,280-dg,20,20,COLOR_BLACK);
	LCDD_DrawRectangle(pLcdBuffer,140-db,285-dg,1,10,COLOR_SNOW);  // Ligne blanche
    LCDD_DrawPixel(pLcdBuffer,139-db,293-dg,COLOR_SNOW); // Quelques pixels...
    LCDD_DrawPixel(pLcdBuffer,141-db,293-dg,COLOR_SNOW);
    LCDD_DrawPixel(pLcdBuffer,138-db,292-dg,COLOR_SNOW);
    LCDD_DrawPixel(pLcdBuffer,142-db,292-dg,COLOR_SNOW);
    LCDD_DisplayBuffer(pLcdBuffer);// et affichage de l'ensemble
	//LCDD_DrawRectangle(pLcdBuffer,220,300,20,20,COLOR_GRAY);
	
	//Affichage Fleche Droite
	LCDD_DrawRectangle(pLcdBuffer,130-db,250-dg,20,20,COLOR_BLACK);
	LCDD_DrawRectangle(pLcdBuffer,140-db,255-dg,1,10,COLOR_SNOW);  // Ligne blanche
    LCDD_DrawPixel(pLcdBuffer,139-db,256-dg,COLOR_SNOW); // Quelques pixels...
    LCDD_DrawPixel(pLcdBuffer,141-db,256-dg,COLOR_SNOW);
    LCDD_DrawPixel(pLcdBuffer,138-db,257-dg,COLOR_SNOW);
    LCDD_DrawPixel(pLcdBuffer,142-db,257-dg,COLOR_SNOW);*/

	
	//Affichage des axes
	
	//Ordonnées
	LCDD_DrawRectangle(pLcdBuffer,20,200,202,2,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,21,202,COLOR_BLACK); // Quelques pixels...
    LCDD_DrawPixel(pLcdBuffer,22,203,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,22,202,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,23,203,COLOR_BLACK);
    LCDD_DrawPixel(pLcdBuffer,21,199,COLOR_BLACK);
    LCDD_DrawPixel(pLcdBuffer,22,198,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,22,199,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,23,198,COLOR_BLACK);

	//Abscisses
	LCDD_DrawRectangle(pLcdBuffer,220,30,2,170,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,219,31,COLOR_BLACK); // Quelques pixels...
    LCDD_DrawPixel(pLcdBuffer,219,32,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,218,32,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,218,33,COLOR_BLACK);
    LCDD_DrawPixel(pLcdBuffer,222,31,COLOR_BLACK);
    LCDD_DrawPixel(pLcdBuffer,222,32,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,223,32,COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer,223,33,COLOR_BLACK);
	
	// Lettres
	
	draw_T(20,193);
	draw_parouvr(20,185);
	draw_deg(16,181);
	draw_C(20,174);
	draw_parferm(20,170);
	
	// Chiffres
	
	diz=val/10;
	uni=val/1-10*diz;
	virg=val*10-100*diz-10*uni;
	//printf("%d %d %d \n", diz, uni, virg);
	
	switch (diz)

{

case 1:
  draw_1(90,300);
  break;
case 2:
  draw_2(90,300);
  break;
case 3:
  draw_3(90,300);
  break;
case 4:
  draw_4(90,300);
  break;
case 5:
  draw_5(90,300);
  break;
case 6:
  draw_6(90,300);
  break;
case 7:
  draw_7(90,300);
  break;
case 8:
  draw_8(90,300);
  break;
case 9:
  draw_9(90,300);
  break;
case 0:
  draw_0(90,300);
  break;
default:
  printf("Ca ne fonctionne pas");
  break;

}
	switch (uni)
{
case 1:
  draw_1(90,280);
  break;
case 2:
  draw_2(90,280);
  break;
case 3:
  draw_3(90,280);
  break;
case 4:
  draw_4(90,280);
  break;
case 5:
  draw_5(90,280);
  break;
case 6:
  draw_6(90,280);
  break;
case 7:
  draw_7(90,280);
  break;
case 8:
  draw_8(90,280);
  break;
case 9:
  draw_9(90,280);
  break;
case 0:
  draw_0(90,280);
  break;
default:
  printf("Ca ne fonctionne pas");
  break;
}

LCDD_DrawRectangle(pLcdBuffer,110,260,3,3,COLOR_BLACK);
LCDD_DrawRectangle(pLcdBuffer,113,260,1,2,COLOR_BLACK);
LCDD_DrawRectangle(pLcdBuffer,114,260,1,1,COLOR_BLACK);

	switch (virg)

{

case 1:
  draw_1(90,255);
  break;
case 2:
  draw_2(90,255);
  break;
case 3:
  draw_3(90,255);
  break;
case 4:
  draw_4(90,255);
  break;
case 5:
  draw_5(90,255);
  break;
case 6:
  draw_6(90,255);
  break;
case 7:
  draw_7(90,255);
  break;
case 8:
  draw_8(90,255);
  break;
case 9:
  draw_9(90,255);
  break;
case 0:
  draw_0(90,255);
  break;
default:
  printf("Ca ne fonctionne pas");
  break;
}

	
	//Récupération de la température 
	abs = HS_Receive();
	signe = HS_Receive();
	
	val = ((float)abs)/2;
	if(signe == 0xFF){val = -val;}
	
	Temp=(int)val;
	
	
	//Affichage de la température
	
	LCDD_DrawRectangle(pLcdBuffer,24,34,196,166,COLOR_WHITE);
	if(compteur<167){
		tabT[compteur]=Temp;
		compteur++;
	}
	else{
		//printf("Boucle else\n");
		i=1;
		while(i<167){
			tabT[i-1]=tabT[i];
			i++;
		}
		tabT[166]=Temp;
		//printf("tabT[i]=%d\n",tabT[167]);
	}
	i=0;
	// affiche la courbe
	while(tabT[i]>=-666 && tabT[i]<=666 && i!=compteur){
		//printf("Test boucle while \n");
		//printf("%d\n",i);
		//printf("%d\n",tabT[i]);
		LCDD_DrawPixel(pLcdBuffer,125-tabT[i],200-i,COLOR_RED);
		i++;
	}
	printf("tableau : %d\n", tabT[0]);
	
	
	
    LCDD_DisplayBuffer(pLcdBuffer);// et affichage de l'ensemble
	//LCDD_DrawRectangle(pLcdBuffer,220,300,20,20,COLOR_GRAY);
}




// 3 fonctions callback pour le TouchScreen
// ----------------------------------------
void TSD_PenPressed(unsigned int x, unsigned int y) {
  unsigned int Couleur=0;
  int i;
  // Sauvegarde des coordonnees de l'impact sur l'ecran 
  XX = x;
  YY = y;
  // Mise a 1 du flag pour alerter le programme principal
  NouvellesValeurs = 1;
  
  if ((x<20) && (y<20) && !SelectScreen) { // Clic dans le coin de l'ecran...
    LCDD_DisplayBuffer(pLcdImage);	// Changement d'image
	SelectScreen = 1;
	}
	else if ((x<230) && (x>180) && (y>260) && (y<310))
	{
		if(!pause){
		pause = 1;
		AffPlanB();
		}
		else {
		pause = 0;
		AffPlanB();
		}
	}
	else {
	SelectScreen = 0;
	
    // utilisation de y comme teinte de couleur, convertie
	// en pleine echelle sur la roue chromatique
	/*Couleur = HSV2RGBColor((int) ((y*9)>>3), 100, 100);*/

	// Effacement de l'ecran avec cette couleur
	// LCDD_Fill(pLcdBuffer,Couleur);
	/*LCDD_Fill(pLcdBuffer,(Couleur >>16)|(Couleur&0x00FF00)|(Couleur&0x0000FF)<<16);*/
	if(!pause)AffPlanB();
	// Attention : Permutation RGB vers BRG car bug dans LCD_Fill qui fonctionne en BRG
	// Autre solution, mais plus lente : 
	// LCDD_DrawRectangle(pLcdBuffer,0,0,240,320,Couleur);

	LCDD_DrawRectangle(pLcdBuffer,0,0,20,20,COLOR_GRAY); // Petit carre de retour
	LCDD_DrawRectangle(pLcdBuffer,5,10,10,1,COLOR_SNOW);  // Ligne blanche
    LCDD_DrawPixel(pLcdBuffer,13,9,COLOR_SNOW); // Quelques pixels...
    LCDD_DrawPixel(pLcdBuffer,13,11,COLOR_SNOW);
    LCDD_DrawPixel(pLcdBuffer,12,8,COLOR_SNOW);
    LCDD_DrawPixel(pLcdBuffer,12,12,COLOR_SNOW);
    LCDD_DisplayBuffer(pLcdBuffer);// et affichage de l'ensemble
  }
}

void TSD_PenMoved(unsigned int x, unsigned int y) {
  // Non utilisee
}
void TSD_PenReleased(unsigned int x, unsigned int y) {
  // Non utilisee
}





void draw_T(int xbas,int ybas)
{
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-2, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-3, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas+1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas-2, COLOR_BLACK);
}

void draw_C(int xbas, int ybas)
{
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas-2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas+1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-2, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-3, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas+1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas-2, COLOR_BLACK);
}

void draw_deg(int xbas,int ybas)
{
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas+1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-2, ybas, COLOR_BLACK);
}

void draw_parouvr(int xbas,int ybas)
{
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas+1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-2, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-3, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas+2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas+1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-6, ybas, COLOR_BLACK);
}

void draw_parferm(int xbas,int ybas)
{
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-2, ybas-2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-3, ybas-2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas-2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-5, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-6, ybas, COLOR_BLACK);
}

void draw_t(int xbas,int ybas)
{
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-2, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-3, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-3, ybas-1, COLOR_BLACK);
}

void draw_s(int xbas,int ybas)
{
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-1, ybas-2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-2, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-3, ybas, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xbas-4, ybas-2, COLOR_BLACK);
}

// Fonctions d'affichage des chiffres


void draw_1(int xcg, int ycg)
{
	int i;
	int j;
	LCDD_DrawPixel(pLcdBuffer, xcg+8, ycg-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+7, ycg-1, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+7, ycg-2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+6, ycg-2, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+6, ycg-3, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+5, ycg-3, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+5, ycg-4, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+4, ycg-4, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+4, ycg-5, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+3, ycg-5, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+3, ycg-5, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+2, ycg-6, COLOR_BLACK);
	LCDD_DrawPixel(pLcdBuffer, xcg+2, ycg-6, COLOR_BLACK);
	for (i=0; i<24; i++)
		{
			LCDD_DrawPixel(pLcdBuffer, xcg+i, ycg-7, COLOR_BLACK);
			LCDD_DrawPixel(pLcdBuffer, xcg+i, ycg-8, COLOR_BLACK);
		}
		for (j=3; j<13; j++)
		{ 
			LCDD_DrawPixel(pLcdBuffer, xcg+22, ycg-j, COLOR_BLACK);
					  LCDD_DrawPixel(pLcdBuffer, xcg+23, ycg-j, COLOR_BLACK);
		}
		LCDD_DrawPixel(pLcdBuffer, xcg+21, ycg-3, COLOR_BLACK);
				  LCDD_DrawPixel(pLcdBuffer, xcg+21, ycg-12, COLOR_BLACK);
}

void go_up (int nbrpixel, int xdeb, int ydeb)
{
	int i;
	for (i=0; i<nbrpixel; i++)
	{
		LCDD_DrawPixel(pLcdBuffer, xdeb-i, ydeb, COLOR_BLACK);
	}
}

void go_down (int nbrpixel, int xdeb, int ydeb)
{
	int i;
	for (i=0; i<nbrpixel; i++)
	{
		LCDD_DrawPixel(pLcdBuffer, xdeb+i, ydeb, COLOR_BLACK);
	}
}

void go_right (int nbrpixel, int xdeb, int ydeb)
{
	int i;
	for (i=0; i<nbrpixel; i++)
	{
		LCDD_DrawPixel(pLcdBuffer, xdeb, ydeb-i, COLOR_BLACK);
	}
}





void go_left (int nbrpixel, int xdeb, int ydeb)
{
	int i;
	for (i=0; i<nbrpixel; i++)
	{
		LCDD_DrawPixel(pLcdBuffer, xdeb, ydeb+i, COLOR_BLACK);
	}
}

void draw_2 (int xcg, int ycg)
{

go_down(2, xcg+20, ycg-12);

go_down(2, xcg+21, ycg-11);

go_left(10, xcg+22, ycg-10);

LCDD_DrawPixel(pLcdBuffer, xcg+21, ycg-1, COLOR_BLACK);

go_up(2, xcg+21, ycg-2);

go_up(2, xcg+20, ycg-3);

go_up(2, xcg+19, ycg-4);

go_up(2, xcg+18, ycg-5);

go_up(2, xcg+17, ycg-6);

go_up(2, xcg+16, ycg-7);

go_up(2, xcg+15, ycg-8);

go_up(3, xcg+14, ycg-9);

go_up(2, xcg+12, ycg-10);

go_up(11, xcg+11, ycg-11);

go_up(7, xcg+9, ycg-12);

go_up(2, xcg+20, ycg-3);

go_left(6, xcg+1, ycg-10);

go_left(6, xcg, ycg-10);

go_down(2, xcg+1, ycg-4);

go_down(2, xcg+2, ycg-3);

go_down(2, xcg+3, ycg-2);

go_down(2, xcg+4, ycg-1);

}
void draw_3 (int xcg, int ycg)
{
 
go_down(3, xcg+20, ycg-1);
go_right(8, xcg+23, ycg-2);
go_right(8, xcg+22, ycg-2);
go_up(2, xcg+22, ycg-10);
go_up(2, xcg+21, ycg-11);
go_up(2, xcg+20, ycg-12);
go_up(5, xcg+19, ycg-13);
go_up(2, xcg+16, ycg-12);
go_up(5, xcg+15, ycg-11);
go_up(3, xcg+14, ycg-10);
go_up(1, xcg+13, ycg-9);
go_up(2, xcg+11, ycg-12);
go_up(6, xcg+10, ycg-13);
go_up(2, xcg+4, ycg-12);
go_up(2, xcg+3, ycg-11);
go_up(2, xcg+2, ycg-10);
go_left(8, xcg+1, ycg-9);
go_left(8, xcg, ycg-9);
go_down(3, xcg+1, ycg-1);
}

void draw_4 (int xcg, int ycg)
{
	go_up(3, xcg+23, ycg-3);
go_right(8, xcg+23, ycg-4);
go_right(8, xcg+22, ycg-4);
go_up(3, xcg+23, ycg-13);
go_up(22, xcg+21, ycg-7);
go_up(22, xcg+21, ycg-8);
go_down(3, xcg+1, ycg-6);
go_down(3, xcg+3, ycg-5);
go_down(3, xcg+5, ycg-4);
go_down(3, xcg+7, ycg-3);
go_down(3, xcg+9, ycg-2);
go_right(12, xcg+12, ycg-1);
go_right(12, xcg+13, ycg-1);
}
/*
void draw_5(int xcg, int ycg)
{
	go_down(3, xcg+21, ycg-1);
	go_right(7, xcg+23, ycg-2);
	go_right(7, xcg+22, ycg-2);
	go_up(2, xcg+22, ycg-9);
	go_up(2, xcg+21, ycg-10);
	go_up(9, xcg+20, ycg-11);
	go_up(7, xcg+19, ycg-12);
	go_up(2, xcg+12, ycg-10);
	go_up(2, xcg+11, ycg-9);
	go_left(6, xcg+10, ycg-8);
	go_left(6, xcg+9, ycg-8);
	go_up(9, xcg+10, ycg-2);
	go_up(9, xcg+10, ycg-1);
	go_right(11, xcg+1, ycg-1);
	go_right(11, xcg, ycg-1);
	go_down(3, xcg, ycg-13);
}
*/
void draw_6(int xcg, int ycg)
{
	go_down(3, xcg, ycg-12);
	go_down(2, xcg+1, ycg-11);
	go_down(2, xcg+2, ycg-10);
	go_down(2, xcg+3, ycg-9);
	go_down(2, xcg+4, ycg-8);
	go_down(2, xcg+5, ycg-7);
	go_down(2, xcg+6, ycg-6);
	go_down(2, xcg+7, ycg-5);
	go_down(2, xcg+8, ycg-4);
	go_down(14, xcg+9, ycg-3);
	go_down(10, xcg+11, ycg-2);
	go_down(4, xcg+14, ycg-1);
	go_right(7, xcg+14, ycg-4);
	go_down(9, xcg+15, ycg-4);
	go_right(7, xcg+15, ycg-4);
	go_right(7, xcg+22, ycg-5);
	go_right(6, xcg+23, ycg-5);
	go_down(6, xcg+16, ycg-11);
	go_down(6, xcg+16, ycg-12);
}

void draw_7(int xcg, int ycg)
{
	go_down(6, xcg, ycg-1);
	go_down(5, xcg, ycg-2);
	go_right(10, xcg, ycg-3);
	go_right(10, xcg+1, ycg-3);
	go_down(2, xcg+2, ycg-11);
	go_down(4, xcg+2, ycg-10);
	go_down(4, xcg+4, ycg-9);
	go_down(6, xcg+6, ycg-8);
	go_down(6, xcg+8, ycg-7);
	go_down(6, xcg+10, ycg-6);
	go_down(4, xcg+14, ycg-5);
	go_down(4, xcg+16, ycg-4);
	go_down(4, xcg+18, ycg-3);
	go_down(4, xcg+20, ycg-2);
	go_down(2, xcg+22, ycg-1);
	go_down(3, xcg+10, ycg-3);
	go_down(3, xcg+9, ycg-12);
	go_right(8, xcg+10, ycg-4);
	go_down(8, xcg+11, ycg-4);
}

void draw_8 (int xcg, int ycg)
{
	go_right(6, xcg+23, ycg-4);
	go_right(8, xcg+22, ycg-3);
	go_right(3, xcg+21, ycg-2);
	go_right(3, xcg+21, ycg-9);
	go_right(3, xcg+20, ycg-1);
	go_right(3, xcg+20, ycg-10);
go_up(5, xcg+19, ycg-1);
go_up(6, xcg+19, ycg-2);
go_up(6, xcg+19, ycg-11);
go_up(5, xcg+19, ycg-12);
go_up(3, xcg+15, ycg-3);
go_up(6, xcg+14, ycg-4);
go_up(4, xcg+13, ycg-5);
go_up(2, xcg+12, ycg-6);
go_up(2, xcg+12, ycg-7);
go_up(4, xcg+13, ycg-8);
go_up(6, xcg+14, ycg-9);
go_up(3, xcg+15, ycg-10);
go_up(3, xcg+10, ycg-3);
go_up(8, xcg+9, ycg-2);
go_up(6, xcg+8, ycg-1);
go_up(3, xcg+3, ycg-3);
go_up(3, xcg+2, ycg-4);
go_right(6, xcg+1, ycg-5);
go_right(5, xcg, ycg-5);
go_right(3, xcg+2, ycg-9);
go_right(3, xcg+3, ycg-10);
go_down(5, xcg+4, ycg-12);
go_down(6, xcg+4, ycg-11);
go_down(3, xcg+8, ycg-10);
}

/*void draw_9 (int xcg, int ycg)
{

go_down(20, xcg+4, ycg-12);

go_down(21, xcg+3, ycg-11);

go_down(3, xcg+2, ycg-10);

go_down(3, xcg+7, ycg-10);

go_down(3, xcg+1, ycg-9);

go_down(3, xcg+8, ycg-9);

go_down(2, xcg, ycg-8);

go_down(2, xcg+10, ycg-8);

go_down(2, xcg, ycg-7);

go_down(2, xcg+10, ycg-7);

go_down(3, xcg, ycg-6);

go_down(3, xcg+10, ycg-6);

go_down(3, xcg, ycg-5);

go_down(3, xcg+8, ycg-5);

go_down(3, xcg, ycg-4);

go_down(3, xcg+7, ycg-4);

go_down(5, xcg+7, ycg-4);

go_down(3, xcg+7, ycg-3);

go_down(3, xcg+2, ycg-3);

go_down(6, xcg+3, ycg-2);

go_down(4, xcg+4, ycg-1);

go_right(10, xcg+23, ycg-1);

go_right(10, xcg+22, ycg-1);

}
*/
/*void draw_0 (int xcg, int ycg)
{

go_down(12, xcg+6, ycg-1);

go_down(16, xcg+4, ycg-2);

go_down(3, xcg+3, ycg-3);

go_down(3, xcg+18, ycg-3);

go_down(3, xcg+2, ycg-4);

go_down(3, xcg+19, ycg-4);

go_down(3, xcg+1, ycg-5);

go_down(3, xcg+20, ycg-5);

go_down(3, xcg, ycg-6);

go_down(3, xcg+21, ycg-6);

go_down(3, xcg, ycg-7);


go_down(3, xcg+21, ycg-7);

go_down(3, xcg+1, ycg-8);

go_down(3, xcg+20, ycg-8);

go_down(3, xcg+2, ycg-9);

go_down(3, xcg+19, ycg-9);

go_down(3, xcg+3, ycg-10);

go_down(3, xcg+18, ycg-10);

go_down(16, xcg+4, ycg-11);

go_down(12, xcg+6, ycg-12);

}

*/

void draw_0 (int xcg, int ycg)
{

go_down(12 , xcg+6, ycg-1);

go_down(16 , xcg+4, ycg-2);

go_down(3 , xcg+3, ycg-3);

go_down(3 , xcg+18, ycg-3);

go_down(3 , xcg+2, ycg-4);

go_down(3 , xcg+19, ycg-4);

go_down(3 , xcg+1, ycg-5);

go_down(3 , xcg+20, ycg-5);

go_down(3 , xcg, ycg-6);

go_down(3 , xcg+21, ycg-6);

go_down(3 , xcg, ycg-7);

go_down(3 , xcg+21, ycg-7);

go_down(3 , xcg+1, ycg-8);

go_down(3 , xcg+20, ycg-8);

go_down(3 , xcg+2, ycg-9);

go_down(3 , xcg+19, ycg-9);

go_down(3 , xcg+3, ycg-10);

go_down(3 , xcg+18, ycg-10);

go_down(16 , xcg+4, ycg-11);

go_down(12 , xcg+6, ycg-12);

}

void draw_5(int xcg, int ycg)
{

go_down(3, xcg+21, ycg-1);

go_right(7, xcg+23, ycg-2);

go_right(7, xcg+22, ycg-2);

go_up(2, xcg+22, ycg-9);

go_up(2, xcg+21, ycg-10);

go_up(9, xcg+20, ycg-11);

go_up(7, xcg+19, ycg-12);

go_up(2, xcg+12, ycg-10);

go_up(2, xcg+11, ycg-9);

go_left(6, xcg+10, ycg-8);

go_left(6, xcg+9, ycg-8);

go_up(9, xcg+10, ycg-2);

go_up(9, xcg+10, ycg-1);

go_right(12, xcg+1, ycg-1);

go_right(12, xcg, ycg-1);

go_down(3, xcg, ycg-13);

}

void draw_9(int xcg, int ycg)
{

go_down(4 , xcg+4, ycg-1);

go_down(5 , xcg+3, ycg-2);

go_down(3 , xcg+2, ycg-3);

go_down(3 , xcg+8, ycg-3);

go_down(3 , xcg+1, ycg-4);

go_down(3 , xcg+8, ycg-4);

go_down(3 , xcg, ycg-5);

go_down(3 , xcg+9, ycg-5);

go_down(2 , xcg, ycg-6);

go_down(2 , xcg+10, ycg-6);

go_down(2 , xcg, ycg-7);

go_down(2 , xcg+10, ycg-7);

go_down(3 , xcg, ycg-11);

go_down(3 , xcg+9, ycg-11);

go_down(3 , xcg+1, ycg-12);

go_down(3 , xcg+8, ycg-12);

go_down(3 , xcg+2, ycg-13);

go_down(3 , xcg+7, ycg-13);

go_down(16 , xcg+3, ycg-14);

go_down(13 , xcg+4, ycg-15);

go_down(5 , xcg+15, ycg-13);

go_down(3 , xcg+18, ycg-12);

go_down(3 , xcg+20, ycg-11);

go_down(3 , xcg+21, ycg-10);

go_left(6 , xcg+22, ycg-9);

go_left(5 , xcg+23, ycg-9);

go_left(2 , xcg+21, ycg-2);

}





























