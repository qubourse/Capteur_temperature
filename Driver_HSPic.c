// Quelques inclusions
#include "Constantes.h"
#include "Delais.h"
#include "Driver_BTN.h"
#include "Driver_LED.h"
#include "Driver_HSPic.h"

void HS_Init(void) {

	TRIS_R = 0x01;
	TRIS_RMI = 0x01;
	TRIS_T = 0x00;
	TRIS_TMO = 0x00;
	LAT_T = 0x00;
	LAT_TMO = 0x00;
	TRIS_Test = 0x00;
	LAT_Test = 0x00;

	ADCON1bits.PCFG0=1;
	ADCON1bits.PCFG1=1;
  	ADCON1bits.PCFG2=1;
  	ADCON1bits.PCFG3=1;
}


void HS_Send_Bit(INT8U val)
{
	LAT_TMO = val;
	LAT_T = 0x01;
	
	while(!PE_R){}
	while(PE_R){}
	
	LAT_T = 0x00;
}

void HS_Send(INT8U val)
{
	int i = 0;
	for(i=0; i<8; i++)
	{
		HS_Send_Bit((val >> i) & 0x01);
	}
}



INT8U HS_Receive_Bit(void)
{
	INT8U data = 0x00;
	LAT_T = 0x01;
	data = PE_RMI;
	LAT_T = 0x00;
	
	return data;
}

INT8U HS_Receive(void)
{
	INT8U data = 0x00;
	int i = 0x00;
	for(i = 0x00; i<8; i++)
	{
		while(!PE_R){};
		data = (data | (HS_Receive_Bit() << i));
		while(PE_R){};
	}
}

