#ifndef	_Driver_HSPic_H

#define _Driver_HSPic_H

// Modes
#define HS_Mode 0x01
#define DEMANDE_DONNEES 0x11

//  Broches Lecture R et RMI
#define TRIS_R		TRISEbits.TRISE0	// R=RE0-PIN8=A(10)-PIN22
#define PE_R 		PORTEbits.RE0		// Signal recu sur RE0
#define TRIS_RMI	TRISEbits.TRISE1	// RMI=RE1-PIN9=A(14)-PIN24
#define PE_RMI 		PORTEbits.RE1		// Signal recu sur RE1

// Broche Ecriture T et TM0
#define TRIS_T		TRISAbits.TRISA0	// T=RA0-PIN2=B(15)-PIN40
#define LAT_T		LATAbits.LATA0		// Bit de controle transmission
#define TRIS_TMO	TRISAbits.TRISA1	// TMO=RA1-PIN3=B(16)-PIN42
#define LAT_TMO		LATAbits.LATA1		// Bit des données transmise
#define TRIS_Test 	TRISAbits.TRISA5
#define LAT_Test	LATAbits.LATA5

// Prototypes des fonctions de ce driver
void HS_Init(void);
void HS_Send_Bit(INT8U);
void HS_Send(INT8U);
INT8U HS_Receive_Bit(void);
INT8U HS_Receive(void);




#endif