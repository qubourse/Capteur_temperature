/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2016                                                          */
/*                                                                          */
/*               Constantes et macros pour carte EasyPIC 6.0                */
/*                                                                          */
/* Constantes.h                                              PIC 18F542     */
/****************************************************************************/

#ifndef	_Constantes_H

#define _Constantes_H

// Redefinition de type
// ********************
#define INT8 char                    
#define INT16 int                     
#define INT8U unsigned char           
#define INT16U unsigned int            
#define BOOLEAN unsigned char           
#define ROM_INT8U const rom unsigned char 
#define ROM_CHAR const rom char

// Constantes generales
// ********************
#define TRUE   1
#define FALSE  0

// Definition de macros
// ********************
#define MAXIMUM(A,B) ((A)>(B)) ? (A) : (B)

// Manipulations de bit sur des variables
// --------------------------------------
//     Deux versions : avec ou sans la lettre T (pour Test)
//                     avec modification de la variable ou sans modification
//     Usage :
//       unsigned char valeur;
//       unsigned char retour;
//
//       valeur = 0b00101110;
//       BitClr(valeur,2);  // Equivalent a valeur=0b00101010
//       retour = BitClrT(valeur,5); // laisse valeur inchangee
//                                   // mais retourne retour=0b00001010

// Negation sur la variable
#define NegT(Var) (~(Var))
#define Neg(Var) ((Var)= ~(Var))

// Mise a 1 d'un bit de la variable
#define BitSetT(Var,BitNbr) ((Var) | 1 << (BitNbr))
#define BitSet(Var,BitNbr) ((Var) |= 1 << (BitNbr))

// Mise a 0 d'un bit de la variable
#define BitClrT(Var,BitNbr) ((Var) & ~(1 << (BitNbr)))
#define BitClr(Var,BitNbr) ((Var) &= ~(1 << (BitNbr)))

// Mise a 0 ou a 1 d'un bit de la variable
#define BitValT(Var,BitNbr,Value) ((Var) & ~(1<<(BitNbr))) | ((Value)<<(BitNbr))
#define BitVal(Var,BitNbr,Value) ((Var) = ((Var) & ~(1<<(BitNbr))) | ((Value)<<(BitNbr)))

// Inversion d'un bit de la variable
#define BitToggleT(Var,BitNbr) ((Var) ^ 1 << (BitNbr))
#define BitToggle(Var,BitNbr) ((Var) ^= 1 << (BitNbr))


#endif