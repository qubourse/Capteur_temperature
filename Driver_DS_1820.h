/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2015                                                          */
/*                                                                          */
/*                                                                          */
/*                     Driver du capteur de temperature                     */
/*                     Dallas DS 1820 en mode One Wire                      */
/*                                                                          */
/* Driver_DS_1820.h                                          PIC 18F542     */
/****************************************************************************/


#ifndef	_Driver_DS_1820_H

#define _Driver_DS_1820_H
#include "Constantes.h"

//================================================================   
//    Connection de la broche OneWire sur carte EasyPIC 6.0
//================================================================

//     Un unique signal, DQ, de transfert de donnees
//     Pour une connection avec le capeteur de temperature DS1820
//     deux possibilites :
//        DQ en RE2 (eventuellement en RA5) 
//        Fonctionnement par time slot 70 us (et reset de 960 us)

#define DQ_IN PORTEbits.RE2
#define DQ_OUT LATEbits.LATE2
#define TRIS_DQ TRISEbits.TRISE2


//================================================================  
// Definition des registres et des constantes du OneWire
//================================================================  
#define OW_RESET_NOT_PRESENT    0
#define OW_RESET_SUCCESS        1
#define DS1820_NOT_PRESENT      0
#define DS1820_PRESENT          1

//================================================================  
// Definition des "ROM Command" du capteur DS1820
//================================================================  
#define DS_SEARCH_ROM_COMMAND       0xF0
#define DS_READ_ROM_COMMAND         0x33
#define DS_MATCH_ROM_COMMAND        0x55
#define DS_SKIP_ROM_COMMAND         0xCC
#define DS_ALARM_SEARCH_COMMAND     0xEC


//================================================================  
// Definition des "Function Command" du capteur DS1820
//================================================================  
#define DS_CONVERT_T_FUNCTION         0x44
#define DS_WRITE_SCRATCHPAD_FUNCTION  0x4E
#define DS_READ_SCRATCHPAD_FUNCTION   0xBE
#define DS_COPY_SCRATCHPAD_FUNCTION   0x48
#define DS_RECALL_E2_FUNCTION         0xB8
#define DS_READ_POWER_SUP_FUNCTION    0xB4


//================================================================  
// Prototypage des fonctions bas niveau "OneWire"
//================================================================  
INT8U OW_Reset(void);
INT8U OW_ReadBit(void);
void OW_WriteBit_0(void);
void OW_WriteBit_1(void);
void OW_WriteBit(INT8U bitval);
INT8U OW_ReadByte(void);
void OW_WriteByte(INT8U Instruction);

//================================================================  
// Prototypage et synonymes des fonctions sur le capteur
//================================================================     
#define DS_SendCmd(Instruction) OW_WriteByte(Instruction)
#define DS_ReadData()           OW_ReadByte()

void DS_Init(void);
INT8U DS_GetTemperature(INT8U *T_Abs, INT8U *T_Sign);

#endif