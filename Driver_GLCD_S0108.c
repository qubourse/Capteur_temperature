/****************************************************************************/
/*  MESNARD Emmanuel                                               ISIMA    */
/*  Octobre 2015                                                            */
/*                                                                          */
/*                                                                          */
/*        Driver pour afficheur Graphique LCD (GLCD)                        */
/*        compatible controleur Samsung S0108, SB0108, KS0108               */
/*                                                                          */
/* Driver_GLCD_S0108.c                                       PIC 18F542     */
/****************************************************************************/

// Compatible avec le Graphical LCD Xiamen Ocular GDM12864B de la carte
// EasyPIC 6.0

#define _Driver_GLCD_S0108_C

#include "p18cxxx.h"
#include "Constantes.h"
#include "Delais.h"
#include "Driver_GLCD_S0108.h"

// Variables globales
//extern ROM_INT8U *Font;    // Police en cours d'utilisation
//extern INT8U FontWidth, FontHeight, FontSpace, FontOffset; 
//extern INT8U CP_Line, CP_Row; // CP : current position

//================================================================   
//      Fonctions d'envoi de commandes ou de donnees
//================================================================

// Les commandes doivent toutes etre separees d'un delai de 5ms (au moins 4.1ms)
// Les quartets ou octets doivent etre separes par un "pulse" sur la 
// broche LCD_E (PORTB(4)), d'une duree d'1 micro seconde (au moins 450 ns)

void GLCD_SendCmd(INT8U Instruction) {
	GLCD_RW = 0;
	GLCD_RS = 0;
	TRIS_GLCD_Data = 0;
	GLCD_Data=Instruction;
	GLCD_Pulse_E();
	Delai_1us();
}

void GLCD_SendData(INT8U Donnee) {
	GLCD_RS = 1;
	GLCD_RW = 0;
	TRIS_GLCD_Data = 0;
	GLCD_Data=Donnee;
	GLCD_Pulse_E();
	Delai_1us();
}

// Generation d'un front sur la broche GLCD_E (PORTB(4)) servant a la validation
void GLCD_Pulse_E(void) { 
  GLCD_E = 1; 
  Delai_1us(); // un peu d'attente
  GLCD_E = 0;  // front descendant effectif
}

// Initialisation du GLCD
void GLCD_Init(void) {
	TRIS_GLCD_Data = 0x00;
	TRIS_GLCD_CS1 = 0;
	TRIS_GLCD_CS2 = 0;
	TRIS_GLCD_RS = 0;
	TRIS_GLCD_RW = 0;
	TRIS_GLCD_E = 0;
	TRIS_GLCD_RST = 0;

	GLCD_CS1 = 0; 
	GLCD_CS2 = 0; // activation simultanee
	GLCD_RS = 0; 
	GLCD_RW = 0;
	GLCD_E = 0;
	GLCD_RST = 0;	

	Delai_10ms(); // attente de 15ms
	Delai_5ms();

	GLCD_CS1 = 1; 
	GLCD_CS2 = 1; // activation simultanee
	Delai_1us();
	GLCD_RST = 1; // Envoi du ReSeT
	Delai_1us();

	while(GLCD_ReadStatus()&0b00010000==0b00010000);

	GLCD_SendCmd(GLCD_DISP_ON);
	/*Delai_1s();
	GLCD_SendCmd(GLCD_DISP_OFF);*/
	
}

//================================================================   
//      Fonctions de lecture de l'etat ou de donnees
//================================================================   

// Lecture de l'etat : 0b Busy 0 Disp Rst 0 0 0 0
//         si Busy = 0 : "Ready", sinon "In operation"
//         si Disp = 0 : "Disp On", sinon "Disp Off"
//         si Rst = 0 : "Normal", sinon "In Reset"
INT8U GLCD_ReadStatus(void) {
INT8U CurrentStatus;

  GLCD_RS = 0;  GLCD_RW = 1; // Status Read
  GLCD_Data = 0xFF;          // Efface le bus
  TRIS_GLCD_Data=0xFF;
  GLCD_E = 1;
  Delai_5us();
  CurrentStatus = GLCD_Data; // Lecture du bus
  GLCD_E = 0;
  Delai_10ms();
  Delai_5ms();
  return (CurrentStatus);
} 
 
// Lecture d'une donnee
INT8U GLCD_ReadData(void) {
INT8U CurrentData;

  GLCD_RS = 1;  GLCD_RW = 1; // Data Read
  GLCD_Data = 0xFF;          // Efface le bus
  TRIS_GLCD_Data=0xFF;
  GLCD_E = 1;
  CurrentData = GLCD_Data;   // Lecture du bus
  GLCD_E = 0;
  return (CurrentData);
} 

//================================================================   
//      Fonctions basiques : position et remplissage   
//================================================================   

// Positionne en X et Y
void GLCD_SetPositionXY(INT8U X_Page, INT8U Y_Column) {   
  GLCD_SendCmd(GLCD_SET_X_ADDRESS|X_Page);
  GLCD_SendCmd(GLCD_SET_Y_ADDRESS|Y_Column);
}   
   
// Remplissage total de l'ecran avec motif
void GLCD_Fill(INT8U Sprite) {
INT8U x;   // Page x sur 3 bits
INT8U y;   // Colonne y sur 6 bits   
  
  GLCD_CS1 = 1;   // Remplissage des deux cotes a la fois
  GLCD_CS2 = 1;   
  for(x=0;x<8;x++){   
    GLCD_SendCmd(GLCD_SET_X_ADDRESS|x);   
    GLCD_SendCmd(GLCD_SET_Y_ADDRESS|0x00);  
    for(y=0;y<64;y++) GLCD_SendData(Sprite);   
  }   
}   

// Effacement ecran
// definit par #define GLCD_ClrScreen() GLCD_Fill(0x00)


//================================================================   
//      Fonctions de gestion d'un pixel : Set et Clr    
//================================================================   

// Dessine un pixel dans l'espace 64 x 128  (XX x YY)
void GLCD_SetPixel(INT8U XX, INT8U YY) {
INT8U x;      // Page x sur 3 bits
INT8U y;      // Colonne y sur 6 bits  
INT8U BitNbr; // Numero de bit sur l'octet considere
INT8U octet;  // Octet de donnee lu a l'adresse indique

  x = XX / 8;      // Calcul la page (position x)
  BitNbr = XX % 8; // Determine le bit dans l'octet considere
  
  if (YY < 64) {
    GLCD_CS1 = 0;  GLCD_CS2 = 1;  // Partie gauche
    y = YY;
  } else {
    GLCD_CS1 = 1;  GLCD_CS2 = 0;  // Partie droite
    y = YY - 64; 
  }
  
  // Lecture de l'ancienne valeur de l'octet a cette position
  GLCD_SetPositionXY(x,y); 
  octet = GLCD_ReadData(); // Forcage de la mise a jour de l'ecriture
  GLCD_SetPositionXY(x,y); // Precedente
  octet = GLCD_ReadData(); // Lecture effective des pixels allumes

  BitSet(octet,BitNbr);    // Mise a 1 du bit
  GLCD_SetPositionXY(x,y); // Mise a jour de la valeur
  GLCD_SendData(octet);
}

void GLCD_ClrPixel(INT8U XX, INT8U YY) {
INT8U x;      // Page x sur 3 bits
INT8U y;      // Colonne y sur 6 bits  
INT8U BitNbr; // Numero de bit sur l'octet considere
INT8U octet;  // Octet de donnee lu a l'adresse indique

  x = XX / 8;      // Calcul la page (position x)
  BitNbr = XX % 8; // Determine le bit dans l'octet considere
  
  if (YY < 64) {
    GLCD_CS1 = 0;  GLCD_CS2 = 1;  // Partie gauche
    y = YY;
  } else {
    GLCD_CS1 = 1;  GLCD_CS2 = 0;  // Partie droite
    y = YY - 64; 
  }
  
  // Lecture de l'ancienne valeur de l'octet a cette position
  GLCD_SetPositionXY(x,y); 
  octet = GLCD_ReadData(); // Forcage de la mise a jour de l'ecriture
  GLCD_SetPositionXY(x,y); // Precedente
  octet = GLCD_ReadData(); // Lecture effective des pixels allumes

  BitClr(octet,BitNbr);    // Mise a 0 du bit
  GLCD_SetPositionXY(x,y); // Mise a jour de la valeur
  GLCD_SendData(octet);
}


//================================================================   
//      Fonctions de gestion des lignes droites   
//================================================================  

void GLCD_Line(INT8U XX1, INT8U YY1, INT8U XX2, INT8U YY2, INT8U Color) {
	int i=0, j=0;
	INT8U larg, haut, pente;
	larg=XX2-XX1;
	haut=YY2-YY2;
	pente=larg/haut;
	for(i=0;i<XX2;i++)
	{
			
	}

}




// Ecriture d'un texte figee, stocke en rom, a la position courante
void GLCD_Write(ROM_CHAR *Texte, INT8U Color) {
  while (*Texte) {
    //GLCD_Chr(*Texte,Color); // Ecriture effective sur LCD
    Texte++;                // Caractere suivant
  }
}

void draw_rec(int x, int y, int l, int h){
	int i, j;
	for(i=y;i<y+l;i++){
		for(j=x;j<x+h;j++){
			GLCD_SetPixel(i, j);
		}
	}
}

void draw_cadre()
{
	int i = 14;
	for(i=12; i<64; i+=6)
	{
		draw_rec(120, i, 1, 8);
		draw_rec(123, i+3, 1, 5);
		draw_rec(9, i, 1, 8);
		draw_rec(9, i+3, 1, 5);
	}
	draw_rec(3, 59, 5, 1);
	draw_rec(5, 63, 1, 3);
	draw_rec(5, 61, 1, 3);
	draw_rec(5, 59, 1, 3);
	draw_rec(5, 60, 1, 1);
	draw_rec(7, 62, 1, 1);
	
	
	
	draw_rec(1, 12, 1, 3);
	draw_rec(1, 10, 1, 3);
	draw_rec(1, 8, 1, 3);
	draw_rec(3, 9, 1, 1);
	draw_rec(3, 11, 1, 1);
	
	draw_rec(5, 12, 1, 3);
	draw_rec(5, 10, 1, 3);
	draw_rec(5, 8, 1, 3);
	draw_rec(7, 9, 1, 1);
	draw_rec(7, 11, 1, 1);
	
}

void draw_tmp(int t,int x){
	int tmpT;
	tmpT = (t-15)*3;
	draw_rec(x+6,64-tmpT , tmpT ,9);
}





