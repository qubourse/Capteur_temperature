/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2016                                                          */
/*                                                                          */
/*           Driver pour Gestion des LEDs sur carte EasyPIC 6.0             */
/*                                                                          */
/* Driver_LED.h                                              PIC 18F542     */
/****************************************************************************/

#ifndef	_Driver_LED_H

#define _Driver_LED_H

#include "Constantes.h"

//================================================================   
//      Fonctions de gestion des LEDs sur carte EasyPIC 6.0
//================================================================

// Format de la connectique :
//   LEDS jaunes : 8 bits sur le port PORTB, en parallele
//   LEDS jaunes : 8 bits sur le port PORTC, en parallele
//   LEDS jaunes : 8 bits sur le port PORTD, en parallele

// Initialisation des ports B, C ou D
// Broches en sortie et extinction des LEDs
#define	LED_B_Init()	{ TRISB = 0x00;  \
                          LATB  = 0x00;  \
                        }
#define LED_C_Init()    { TRISC = 0x00;  \
                          LATC  = 0x00;  \
                        }
#define LED_D_Init()    { TRISD = 0x00;  \
                          LATD  = 0x00;  \
                        }                        
// Affichage de la valeur sur les LEDs
#define LED_B(Value)    LATB = (Value)
#define LED_C(Value)    LATC = (Value)
#define LED_D(Value)    LATD = (Value)

// Allumage ou Extinction d'un bit des LEDs
#define LED_B_Bit(BitNbr, Value) (LATB=BitValT(PORTB,(BitNbr),(Value)))
#define LED_C_Bit(BitNbr, Value) (LATC=BitValT(PORTC,(BitNbr),(Value)))
#define LED_D_Bit(BitNbr, Value) (LATD=BitValT(PORTD,(BitNbr),(Value)))

// Allumage d'un bit des LEDs
#define LED_B_Bit_On(BitNbr) (LATB=BitSetT(PORTB,(BitNbr)))
#define LED_C_Bit_On(BitNbr) (LATC=BitSetT(PORTC,(BitNbr)))
#define LED_D_Bit_On(BitNbr) (LATD=BitSetT(PORTD,(BitNbr)))

// Extinction d'un bit des LEDs
#define LED_B_Bit_Off(BitNbr) (LATB=BitClrT(PORTB,(BitNbr)))
#define LED_C_Bit_Off(BitNbr) (LATC=BitClrT(PORTC,(BitNbr)))
#define LED_D_Bit_Off(BitNbr) (LATD=BitClrT(PORTD,(BitNbr)))

// Basculement d'un bit des LEDs
#define LED_B_Bit_Toggle(BitNbr) (LATB=BitToggleT(PORTB,(BitNbr)))
#define LED_C_Bit_Toggle(BitNbr) (LATC=BitToggleT(PORTC,(BitNbr)))
#define LED_D_Bit_Toggle(BitNbr) (LATD=BitToggleT(PORTD,(BitNbr)))

// Basculement de toutes les LEDs
#define LED_B_Toggle()  LATB = NegT(PORTB)
#define LED_C_Toggle()  LATC = NegT(PORTC)
#define LED_D_Toggle()  LATD = NegT(PORTD)
#endif
