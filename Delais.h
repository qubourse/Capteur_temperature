/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2016                                                          */
/*                                                                          */
/*                 Fonction de gestion des delais pour carte                */
/*                 MikroElectronika EasyPIC 6.0 avec Quartz de 8MHz         */
/*                                                                          */
/* Delais.h                                                  PIC 18F542     */
/****************************************************************************/

#ifndef	_DELAIS_H

#define _DELAIS_H
#include <delays.h>
#include "Constantes.h"

//================================================================   
//      Fonctions de gestion des delais sur carte EasyPIC 6.0
//================================================================

// Les delais disponibles sont fixes
//    Delai_0_5us()
//    Delai_1us() a Delai_9us()
//    Delai_10us() a Delai_90us()
//    Delai_100us() a Delai_900us()
//    Delai_1ms() a Delai_9ms()
//    Delai_10ms() a Delai_90ms()
//    Delai_100ms() a Delai_900ms()
//    Delai_1s()

// Usage : Cumuler les appels des fonctions, sequentiellement
// Exemple :   Delai_10ms(); Delai_3ms();
//             provoquera l'attente de 13ms


//================================================================  
//               Constantes et macros pour les delais
//================================================================  

// Definition de la frequence d'horloge
#define Frequence_CLK_Mhz 8

// 1 cycle instruction = 4 cycles d'horloge
// Nombre de cycles pour faire 1 micro-seconde
#define N1us (Frequence_CLK_Mhz/4)
// Sur EasyPIC 6.0 avec Quartz de 8MHz, N1us vaut 2

#define Delai_0_5us()   Delay1TCY()

#define Delai_1us()    {Delay1TCY();Delay1TCY()}
#define Delai_2us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY()}
#define Delai_3us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY()}
#define Delai_4us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY()}
#define Delai_5us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY()}
#define Delai_6us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY()}
#define Delai_7us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY()}
#define Delai_8us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY()}
#define Delai_9us()    {Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY();Delay1TCY();Delay1TCY();\
                        Delay1TCY();Delay1TCY()}

#define Delai_10us()    Delay10TCYx(N1us)
#define Delai_20us()    Delay10TCYx(2*N1us)
#define Delai_30us()    Delay10TCYx(3*N1us)
#define Delai_40us()    Delay10TCYx(4*N1us)
#define Delai_50us()    Delay10TCYx(5*N1us)
#define Delai_60us()    Delay10TCYx(6*N1us)
#define Delai_70us()    Delay10TCYx(7*N1us)
#define Delai_80us()    Delay10TCYx(8*N1us)
#define Delai_90us()    Delay10TCYx(9*N1us)

#define Delai_100us()   Delay100TCYx(N1us)
#define Delai_200us()   Delay100TCYx(2*N1us)
#define Delai_300us()   Delay100TCYx(3*N1us)
#define Delai_400us()   Delay100TCYx(4*N1us)
#define Delai_500us()   Delay100TCYx(5*N1us)
#define Delai_600us()   Delay100TCYx(6*N1us)
#define Delai_700us()   Delay100TCYx(7*N1us)
#define Delai_800us()   Delay100TCYx(8*N1us)
#define Delai_900us()   Delay100TCYx(9*N1us)


#define Delai_1ms()     Delay1KTCYx(N1us)
#define Delai_2ms()     Delay1KTCYx(2*N1us)
#define Delai_3ms()     Delay1KTCYx(3*N1us)
#define Delai_4ms()     Delay1KTCYx(4*N1us)
#define Delai_5ms()     Delay1KTCYx(5*N1us)
#define Delai_6ms()     Delay1KTCYx(6*N1us)
#define Delai_7ms()     Delay1KTCYx(7*N1us)
#define Delai_8ms()     Delay1KTCYx(8*N1us)
#define Delai_9ms()     Delay1KTCYx(9*N1us)

#define Delai_10ms()    Delay10KTCYx(N1us)
#define Delai_20ms()    Delay10KTCYx(2*N1us)
#define Delai_30ms()    Delay10KTCYx(3*N1us)
#define Delai_40ms()    Delay10KTCYx(4*N1us)
#define Delai_50ms()    Delay10KTCYx(5*N1us)
#define Delai_60ms()    Delay10KTCYx(6*N1us)
#define Delai_70ms()    Delay10KTCYx(7*N1us)
#define Delai_80ms()    Delay10KTCYx(8*N1us)
#define Delai_90ms()    Delay10KTCYx(9*N1us)


#define Delai_100ms()   Delay10KTCYx(10*N1us)
#define Delai_200ms()   Delay10KTCYx(20*N1us)
#define Delai_300ms()   Delay10KTCYx(30*N1us)
#define Delai_400ms()   Delay10KTCYx(40*N1us)
#define Delai_500ms()   Delay10KTCYx(50*N1us)
#define Delai_600ms()   Delay10KTCYx(60*N1us)
#define Delai_700ms()   Delay10KTCYx(70*N1us)
#define Delai_800ms()   Delay10KTCYx(80*N1us)
#define Delai_900ms()   Delay10KTCYx(90*N1us)

#define Delai_1s()      Delay10KTCYx(100*N1us)


#endif
