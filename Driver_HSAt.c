// Quelques inclusions
#include "Driver_HSAt.h"


void HS_Init(void) {
	
	PIO_Configure(&COM_R,1);
	PIO_Configure(&COM_RMI,1);
	PIO_Configure(&COM_T,1);
	PIO_Configure(&COM_TMO,1);
	
	PIO_Clear(&COM_T);
	PIO_Clear(&COM_TMO);
}


void HS_Send_Bit(short int val)
{
	if(val & 0x01){
		PIO_Set(&COM_TMO);
	}
	else{
		PIO_Clear(&COM_TMO);
	}
	PIO_Set(&COM_T);
	
	while(!PIO_Get(&COM_R)){}
	while(PIO_Get(&COM_R)){}
	
	PIO_Clear(&COM_T);
}

void HS_Send(short int val)
{
	int i = 0;
	for(i=0; i<8; i++)
	{
		HS_Send_Bit((val >> i) & 0x01);
	}
}

short int HS_Receive_Bit(void)
{
	short int data = 0x00;
	PIO_Set(&COM_T);
	data = PIO_Get(&COM_RMI);
	PIO_Clear(&COM_T);
	
	return data;
}

short int HS_Receive(void)
{
	short int data = 0x00;
	int i = 0x00;
	for(i = 0x00; i<8; i++)
	{
		while(!PIO_Get(&COM_R)){};
		data = (data | (HS_Receive_Bit() << i));
		while(PIO_Get(&COM_R)){};
	}
	return data;
}

