/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2016                                                          */
/*                                                                          */
/*                                                                          */
/*                     Driver pour Expander de Ports (PE)                   */
/*                     MicroChip MCP 23S17 en mode SPI                      */
/*                                                                          */
/* Driver_PE_23S17.h                                         PIC 18F542     */
/****************************************************************************/


#ifndef	_Driver_PE_23S17_H

#define _Driver_PE_23S17_H

//================================================================  
// Definition des Registres de l'Expander pour IOCON.BANK = 0
//================================================================  
#define IODIRA   	0x00
#define IODIRB   	0x01
#define IPOLA    	0x02
#define IPOLB    	0x03
#define GPINTENA 	0x04
#define GPINTENB 	0x05
#define DEFVALA  	0x06
#define DEFVALB  	0x07
#define INTCONA  	0x08
#define INTCONB  	0x09
#define IOCONA   	0x0A
#define IOCONB   	0x0B
#define GPPUA    	0x0C
#define GPPUB    	0x0D
#define INTFA    	0x0E
#define INTFB    	0x0F
#define INTCAPA  	0x10
#define INTCAPB  	0x11
#define GPIOA    	0x12
#define GPIOB    	0x13
#define OLATA    	0x14
#define OLATB    	0x15

//================================================================   
//    Connection des broches de l'Expander sur carte EasyPIC 6.0
//================================================================

//  Broches du port SPI : 
//    SCK  =SW6(3)=PORTC(3)=RC(3)
//    MISO =SW6(4)=PORTC(4)=RC(4)
//    MOSI =SW6(5)=PORTC(5)=RC(5)

//  Broches Selection et Reset

 #define TRIS_PE_CS		TRISAbits.TRISA2 // r/w CS
 #define PE_CS			LATAbits.LATA2 // ecrire CS
 #define TRIS_PE_RST 	TRISAbits.TRISA3 // r/w RESET
 #define PE_RST 		LATAbits.LATA3 // ecrire RESET
     
// Adresse physique du SPI de l'expander : 
#define PE_ADDRESS 0b000 // A(2:0) = 000

// Prototypes des fonctions de ce driver
void PE_Init(void);
void PE_SendCmd(INT8U Instruction, INT8U Parametre);
void PE_PORT0SetMode(INT8U Orientation);
void PE_PORT1SetMode(INT8U Orientation);
void PE_PORT0WriteByte(INT8U Valeur);
void PE_PORT1WriteByte(INT8U Valeur);
#endif