/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2015                                                          */
/*                                                                          */
/*                     Driver du capteur de temperature                     */
/*                     Dallas DS 1820 en mode One Wire                      */
/*                                                                          */
/* Driver_DS_1820.c                                          PIC 18F542     */
/****************************************************************************/


#define _Driver_DS_1820_C

#include <p18cxxx.h>
#include "Constantes.h"
#include "Delais.h"
#include "Driver_DS_1820.h"


//================================================================   
//      Fonctions elementaires de controle du OneWire
//================================================================

// Le OneWire est un bus monofilaire DQ, bidirectionnel.
// Il faut donc en permanence changer le sens d'orientation du signal DQ


// Fonction de Reset
INT8U OW_Reset(void) {
INT8U Presence;

  TRIS_DQ = 1;      // Par defaut, toujours laisser DQ en entree,
                    // afin d'obtenir un "1" sur le fil par Pull up
  DQ_OUT = 0;
  // Creation du Pulse sur la ligne
  TRIS_DQ = 0;      // Cette fois, la ligne DQ passe a 0 (DQ en sortie)
  Delai_400us();    // Attente de 480 us
  Delai_80us();
  TRIS_DQ = 1;      // Relachement de la ligne DQ pour laisser les esclaves
  Delai_100us();    // Attente de 100 us pour centrage au milieu des reponses
  // Test de presence d'esclaves sur la ligne
  Presence = ((DQ_IN == 0) ? OW_RESET_SUCCESS : OW_RESET_NOT_PRESENT);
  Delai_300us();  // Attente de 380 us pour finir le cycle de 960 us
  Delai_80us();
  
  return(Presence);
}

// Fonctions de lecture et d'ecriture de bits, en LSb
 INT8U OW_ReadBit(void) {
 INT8U ValeurLue;
  DQ_OUT = 0;
  TRIS_DQ = 0;      // Debut du pulse
  Delai_6us();      // Attente de 6 us
  TRIS_DQ = 1;      // Fin du pulse avec relachement de la ligne DQ
  Delai_9us();      // Attente de 9 us avant echantillonnage
  ValeurLue = DQ_IN;// Lecture du bus DQ
  Delai_50us();     // Attente de 55 us pour finir le time slot
  Delai_5us();
  return(ValeurLue);
 }
  
 void OW_WriteBit_0(void) {
	DQ_OUT = 0;
	TRIS_DQ = 0;
	Delai_60us();
	TRIS_DQ = 1;
	Delai_10us();
 }
 
void OW_WriteBit_1(void) {
	DQ_OUT = 0;
	TRIS_DQ = 0;
	Delai_6us();
	TRIS_DQ = 1;
	Delai_60us();
	Delai_4us();
}
 
void OW_WriteBit(INT8U bitval) {
	if (bitval)
	{
		OW_WriteBit_1();
	}
	else if (0==bitval)
	{
		OW_WriteBit_0();
	}
}
// Fonctions de lecture et d'ecriture d'octets, en LSb
INT8U OW_ReadByte(void) {
  INT8U i;
  INT8U DonneLue;
  INT8U ValeurBit;

  DonneLue = 0;
  ValeurBit = 0;

  for(i=0;i<8;i++) {
		ValeurBit = OW_ReadBit();
        // Les donnees et instructions sont transmises en LSb (bit le plus faible en premier)
  		// Donc, installation du bit lu a gauche, avec decalage a droite d'un cran a chaque tour !
  		DonneLue = DonneLue >> 1;
		DonneLue |= ((ValeurBit) ? 0x80 : 0x00);
  }
  return(DonneLue);
}
 
 void OW_WriteByte(INT8U Instruction) {
	INT8U i;
	for(i=1; i<=8; i++)
	{
		OW_WriteBit(Instruction & 0b00000001);
		Instruction = Instruction >> 1;
	}
 }
 
//================================================================  
// Prototypage des fonctions specifiques au capteur DS1820
//================================================================  

void DS_Init(void) {
// Configuration des broches RE2 (utilise pour DQ, eventuellement RA5)
// en mode de fonctionnement NUMERIQUE (car par defaut,
// ces broches sont analogiques)
// Cf. Datasheet PIC18F452 en paragraphe 17 sur les entrees sorties A/D
// Note de bas de Page 182 :On any device RESET, the port pins that are
// multiplexed with analog functions (ANx) are forced to be an analog input.

  TRIS_DQ = 1;      // Par defaut, toujours laisser DQ en entree,
                    // afin d'obtenir un "1" sur le fil par Pull up
  DQ_OUT = 0;
  ADCON1bits.PCFG0=1;
  ADCON1bits.PCFG1=1;
  ADCON1bits.PCFG2=1;
  ADCON1bits.PCFG3=1;
}
 

// Obtention de la temperature (2 octets), sur le capteur
INT8U DS_GetTemperature(INT8U *T_Abs, INT8U *T_Sign) {
  INT8U Presence;
  
  // Toute transaction commence systematiquement par Reset
  Presence = OW_Reset();
  
  if (Presence == OW_RESET_SUCCESS) { // Controle de presence du capteur
    // Lancement de la procedure de demande de conversion
    DS_SendCmd(DS_SKIP_ROM_COMMAND); // Pas de commande ROM
    DS_SendCmd(DS_CONVERT_T_FUNCTION);
    Delai_500ms(); // Attente necessaire a la conversion
    
    // Nouvelle transaction : demande de lecture des donnees
    Presence = OW_Reset();
    if (Presence == OW_RESET_SUCCESS) { // Re-verification du bon fonctionnement
      DS_SendCmd(DS_SKIP_ROM_COMMAND);
      DS_SendCmd(DS_READ_SCRATCHPAD_FUNCTION);
      Delai_400ms(); // Attente necessaire a l'acces RAM
      *T_Abs = DS_ReadData();
      *T_Sign = DS_ReadData();
      // Temperature[0] = Valeur absolue du double de la temperature
      // Temperature[1] = Signe de la temperature
      // Pour les nombres n�gatifs, faire +1 au complement de la valeur
      if ((*T_Sign) == 0xFF) {
          *T_Abs = ~*T_Abs + 1;
      }
    }
  }
  return(Presence);
}



