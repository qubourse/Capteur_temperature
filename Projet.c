// Configuration physique du composant
#pragma config OSC = HS, OSCS = OFF
#pragma config PWRT = OFF, WDT = OFF
#pragma config BOR = OFF, LVP = OFF 

// Inclusions des constantes et des fonctions de delais
#include <p18cxxx.h>
#include "Constantes.h"
#include "Delais.h"
#include "Driver_LED.h"
#include "Driver_BTN.h"
#include "Driver_HSPic.h"
#include "Driver_GLCD_S0108.h"

#include "Driver_COG_2x16.h"
#include <stdlib.h>  // pour itoa
#include "Driver_DS_1820.h"

#include "Driver_PE_23S17.h"

void main(void)
{
	int x = 13;
  int c=21;
	char i;
  char texte[8];
  char texte1[8];

	INT8U Presence;
	INT8U T_Abs,T_Sign; // Temperature
  HS_Init();
  DS_Init();    // Initialisation du capteur Dallas 1820
  LED_B_Init(); // Initialisation des LEDS
  LED_C_Init();
  LED_D_Init();
  LED_D(0xaa);
  TRISD=0;
  
  PE_Init();
  PE_PORT0SetMode(0x00); //PORT0&1 réglés en sortie
  PE_PORT1SetMode(0x00);
  
  // Initialisations des LEDs et COG
  COG_Init();
  COG_SendCmd(COG_NO_CURSOR); // pas de curseur
  
  GLCD_Init();
	GLCD_Fill(0b00000000);
  // Ecriture en premiere ligne d'un texte constant
  COG_PrintAt(0,0,"* Temperature!  *");  
  i=0;
  COG_WriteAt(1,12,"C");
  while(1) {
    // 2eme ligne : un nombre qui s'incremente...
	
	 DS_GetTemperature(&T_Abs, &T_Sign);
	 PORTD = T_Abs;
	 LED_B(T_Abs);
	 HS_Send(T_Abs);
	 HS_Send(T_Sign);

	//glcd
	draw_cadre();
	if(c>4){
		draw_tmp(T_Abs/2,x);
		x+=10;
		c=1;
	}
	Delai_500ms();
	c++;
	if(x>109)
	{
		x=13;
		GLCD_Fill(0x00);
		draw_cadre();
	}
	 
	/*COG_WriteAt(0,0,"* Driver OK !  *"); 
	 
    COG_WriteAt(1,0,"Temp =         "); // A noter : WRITE pour une chaine constante
	itoa(T_Abs >> 1,texte);
	COG_PrintAt(1,8,texte);            // A noter : et PRINT pour une chaine variable
	
	COG_ChrAt(1, 11, 0b11011111);
	
	T_Abs=i++;
	itoa(T_Abs >> 1,texte);
	COG_PrintAt(1,8,texte); 
	
	COG_WriteAt(1,12,"C");*/
	
	
	
	Delai_1s();
	
  }
	
	
	/*
	
	INT8U var;	
	HS_Init();
	while(1){
		var = PE_R;
		LAT_Test = var;
	}
	*/
	
	
}

/*void main(void){
		HS_Init();
		LAT_T = 0x01;
		LAT_TMO = 0x01;
		Delai_1s();
		LAT_T = 0x00;
		LAT_TMO = 0x00;
		Delai_1s();
}*/

/*void main(void)
{
	INT8U valeur;
	INT8U temperature;
	while(1)
	{
		if(HS_Mode == 0x01)
		{
			Delai_1s();
			HS_Send(DEMANDE_DONNEES);
			while(!PE_R){}
			valeur = HS_Receive();	
		}
		else
		{
			while(!PE_R){}
			valeur = HS_Receive();
			if(valeur == DEMANDE_DONNEES)
			{
				HS_Send(temperature);
			}
		}
	}
}*/




