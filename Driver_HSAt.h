#ifndef	_Driver_HSAt_H
#define _Driver_HSAt_H

#include <board.h>
#include <pio/pio.h>
#include <rtt/rtt.h>

// Modes																											Demo_Cap9
#define HS_Mode 0x01
#define DEMANDE_DONNEES 0x11

//  Broches Lecture R et RMI
#define PIN_COM_R	{AT91C_PIO_PB15,AT91C_BASE_PIOB,AT91C_ID_PIOABCD,PIO_INPUT,PIO_DEFAULT}
#define PIN_COM_RMI	{AT91C_PIO_PB16,AT91C_BASE_PIOB,AT91C_ID_PIOABCD,PIO_INPUT,PIO_DEFAULT}


// Broche Ecriture T et TM0
#define PIN_COM_T	{AT91C_PIO_PA10, AT91C_BASE_PIOA, AT91C_ID_PIOABCD, PIO_OUTPUT_0, PIO_DEFAULT}
#define PIN_COM_TMO	{AT91C_PIO_PA14, AT91C_BASE_PIOA, AT91C_ID_PIOABCD, PIO_OUTPUT_0, PIO_DEFAULT}

static const Pin COM_R = PIN_COM_R;
static const Pin COM_RMI = PIN_COM_RMI;
static const Pin COM_T = PIN_COM_T;
static const Pin COM_TMO = PIN_COM_TMO;

// Prototypes des fonctions de ce driver
void HS_Init(void);
void HS_Send_Bit(short int);
void HS_Send(short int);
short int HS_Receive_Bit(void);
short int HS_Receive(void);

#endif