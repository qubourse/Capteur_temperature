/****************************************************************************/
/*  MESNARD Emmanuel                                              ISIMA     */
/*  Septembre 2016                                                          */
/*                                                                          */
/*           Driver pour Gestion des BTNs sur carte EasyPIC 6.0             */
/*                                                                          */
/* Driver_BTN.h                                              PIC 18F542     */
/****************************************************************************/

#ifndef	_Driver_BTN_H

#define _Driver_BTN_H

#include "Constantes.h"

//================================================================   
//      Fonctions de gestion des BTNs sur carte EasyPIC 6.0
//================================================================

// Initialisation des ports B, C ou D
// Attention : initialisation de la valeur requise
#define	BTN_B_Init()	{ LATB  = 0X00;  \
                          TRISB = 0xFF;  \
                        }
#define BTN_C_Init()    { LATC  = 0x00;  \
                          TRISC = 0xFF;  \
                        }
#define BTN_D_Init()    { LATD  = 0x00;  \
                          TRISD = 0xFF;  \
                        }
                        
// Lecture de la valeur de tous les boutons sur les ports
#define BTN_B()    PORTB
#define BTN_C()    PORTC
#define BTN_D()    PORTD

// Lecture d'un bit, donc d'un bouton, sur l'un des ports
#define BTN_B_Bit(BitNbr) ( (PORTB>>(BitNbr)) & 0x01 )
#define BTN_C_Bit(BitNbr) ( (PORTC>>(BitNbr)) & 0x01 )
#define BTN_D_Bit(BitNbr) ( (PORTD>>(BitNbr)) & 0x01 )					
						
#endif
